import React from "react";
import Navegation from "../Navegation/Navegation";
import GoogleMaps from "simple-react-google-maps";
import "./Map.scss";
import lupa from "../../ImgAppLucky/lupa.png";
export default function Map() {

  
  return (

    <div className="buscador">
      <div className="input-buscador">
                <input className="inputBuscador" type="text" placeholder=" ¿Qué estás buscando?" />
                <span classname="input-group-buscar">
                    <span className=" searcher-icon fas fa-buscar"><img className="imagen-lupa" src={lupa}></img></span>
                </span>
            </div>


      <div className="container-mapa">
        <GoogleMaps
          apiKey={"AIzaSyB7VM4e-1REQNV1_S9lmSGuQ4Hg0Y-xR5w"}
          style={{ height: "550px", width: "380px" }}
          zoom={12}
          center={{
            lat: 40.4167,
            lng: -3.70325,
          }}
          
           markers={{ lat: 40.4588114, lng: -3.6946848}, {lat: 40.432857, lng:-3.660869}, {lat:40.3818135,lng:-3.6543583}}
        />
      </div>

      <Navegation></Navegation>
    </div>
  );
}
