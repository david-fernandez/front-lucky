import React from 'react';
import { useForm } from 'react-hook-form';
import { API } from '../../../Shared/Servicios/Api';


function UploadImage() {

    const onSubmit = (e) => {
        // ¿ Para que no te lleve a otro sitio ?
        e.preventDefault();

        const imageForm = document.getElementById("profileImageform");

        console.log(imageForm)

        const formData = new FormData(imageForm);

        console.log(formData)

        const userToken = localStorage.getItem("token");
        if (!userToken) {
            return (window.location.href = "/Usuarios/login.html");
        }

        fetch(process.env.REACT_APP_BACK_URL + "perfilUsuario/profileImage", {
            method: "POST",
            body: formData,
            headers: {
                Authorization: "Bearer " + userToken,
            },
        })
            .then((respuesta) => {
                alert("Tu foto se ha actualizado correctamente");
                console.log(respuesta.status);
            })
            .catch((err) => {
                console.log(err);
            });
    };


    return (
        <div>
            <form encType="multipart/form-data" onSubmit={onSubmit}   id="profileImageform">
                <input type="file" name="avatar"/>
                <button>Enviar</button>
            </form>
        </div>
    );

}


export default UploadImage;