import React, { useState, useEffect } from 'react';
import Navegation from '../Navegation/Navegation';
import masSecundario from '../../ImgAppLucky/iconosAnimales/masSecundario.png';
import './MisMascotas.scss';
import flechaRosaDerecha from '../../ImgAppLucky/flechaRosaDerecha.png';
import adopcionLineasVerticales from '../../ImgAppLucky/adopcionLineasVerticales.png';
import perroRocky from '../../ImgAppLucky/perroRocky.png';
import catSimba from '../../ImgAppLucky/catSimba.png';
import conejouii from '../../ImgAppLucky/conejouii.png';
import { Carousel } from 'primereact/carousel';
import Perro from '../../ImgAppLucky/iconosAnimales/Perro2.png';
import ave from '../../ImgAppLucky/iconosAnimales/ave.png';
import { API } from '../../Shared/Servicios/Api';
import { Link } from 'react-router-dom';

// import {Carousel} from 'react-elastic-carousel';
export default function MisMascotas() {
    const profile = JSON.parse(localStorage.getItem('usuario'));
    const [allPets, setAllPets] = useState({});

    useEffect(() => {

        API.get(process.env.REACT_APP_BACK_URL + 'api/datosMascota').then(res => {

            setAllPets(res.data);


        })

    }, [])

    const finalAllPets = allPets && allPets;
    console.log(finalAllPets)

    const listPets = [];

    for (let i = 0; i < finalAllPets.length; i++) {
        listPets.push(
            <div key={i} className="petContainerSimba" >
                <img className="imagesSimba" src={finalAllPets[i].img} alt="" />

                <div className="simba-container" >

                    <div className="simba"   >
                        <h3 >{finalAllPets[i].nombre}</h3>
                    </div>
                    <div className="ubicacionSimba" >
                        <h6>Madrid 15km</h6>

                    </div>
                </div>
            </div>

        );

    }


    const [mascotas] = useState([
        {
            nombre: "Apolo",
            icono: Perro
        },
        {
            nombre: "Kiko",
            icono: Perro
        },
        {
            nombre: "Dali",
            icono: Perro

        }
    ]);


    const itemTemplate = (mascota) => <div className="b-primereact-carousel-iconos-adoptados " >


        <img className="b-primeract-carousel-iconos-image"
            src={mascota.icono} alt="imagenes tus animales adoptados" />

        <span className="b-primereact-carousel-nombre-container" >
            <span> {mascota.nombre} </span>
        </span>

    </div>


    return (
        <div className="main-container" >
            <div className="input-group">
                <input className="inputSearcher" type="text" placeholder="Buscar" />
                <span class="input-group-addon">
                    <span class=" searcher-icon fas fa-search"></span>
                </span>
            </div>
            <div className="span-container" >


                <span className="span1"> Mis mascotas <img className="imageMore" src={masSecundario} alt="" /> </span>


                <span className="span2"> Accede al perfil de tus mascotas  </span>




            </div>

            <div style={{margin:'auto'}} >
                <Carousel className="b-primereact-carousel" value={mascotas} itemTemplate={itemTemplate} > </Carousel>
            </div>

            <div className="estado-adopcion-container" >
                <div className="adopcion-container-header" >
                    Estado de la adopción
                </div>
                <div className="adopcion-container-image" >
                    <button className="button-arrow-container" >
                        <img className="arrow-image" src={flechaRosaDerecha} alt="" />
                    </button>
                </div>
            </div>

            <div className="animales-adopcion-container" >
                <div className="animales-adopcion-header-container" >
                    Animales en adopción
                </div>
                <Link to="/filtro">
                    <div className="adopcion-image-container" >
                        <img className="adopcion-image" src={adopcionLineasVerticales} alt="" />
                    </div>
                </Link>
            </div>

            <div className="gallery-container" >

                <div>
                    {listPets}
                </div>

                <Navegation></Navegation>

            </div>


           
            
             



        </div>

    )
}
